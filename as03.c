#include <stdio.h>
int main() {
   printf("Assignment 3: Simple Interest Calculator!");
   /* 
      int is avoided in the program as it range is too small for real amt of money,
      plus it cant take decimal input & etc
      float is used instead of Double to be more memory Efficient and as
      We dont require that amount of precision of double here as we rounding it off 2 decimal points
   */
   float rate, time, prin, siinterest;
   printf("\n\nEnter the Principal Amount Of Money: ");
   scanf("%f", &prin);
   printf("\nEnter the Rate Of Interest: ");
   scanf("%f", &rate);
   printf("\nEnter the Time Borrowed: ");
   scanf("%f", &time);
   siinterest = (prin*rate*time)/100;
   printf("\nThe Simple Interest Amount is: %.2f",siinterest);
   
   return 0;
}