#include <stdio.h>

// Global Variables
int num1,num2,diff;


// No pass by value is used as the variables are Global
// Hence accessible to each function
int differ(){
    // stdlib.h with abs() can be used to do the same
    // instead of using if
    if (num1>=num2){
        diff=num1-num2;
    } else{
        diff=num2-num1;
    }
    return 0;
}
int main (){
    printf("Assignment 5: Program to Calculate Difference of Two Numbers");
    printf("\nEnter the First number: ");
    scanf("%ld",&num1);
    printf("\n5Enter the Second number: ");
    scanf("%ld",&num2);
    // Calls the differ function
    differ();
    printf("The Difference between two is: %ld",diff);
    return 0;
}