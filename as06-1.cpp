#include <iostream>
using namespace std;

int main(){
    int a,b;
    cout<<"Assignment 6: Q.1 - Finding Greatest Number between 2";
    cout<<"\nEnter the two numbers: ";
    cin>>a>>b;
    if (a>=b){
        // Checking for Equality of two
        if (a==b){
            cout<<"\na & b are Equal";
        }
        // Else is executed is a>b
        else{
        cout<<"\na is Greater than b";
        }
    }
    // else is executed if b>a
    else{
        cout<<"\nb is Greater than a";
    }
    return 0;
}