#include <stdio.h>
#include <string.h>

// In C, struct is visible as public by default
// Whereas in C++, class is private by default
struct employee{
    char name[50];
    char address[100];
    char designation[50];
    int yearsworked;
    int salary;
};
int runner(){
    // As We can see any newly created function can access the content of struct by default
    // Without any manual permission
    struct employee taylor;
    // taylor is random name I have chosen for the Employee Name

    // Data
    strcpy(taylor.name,"Jason Taylor");
    strcpy(taylor.address,"Retina Towers, Square Park, USA");
    strcpy(taylor.designation,"Chief Sales Officer");
    taylor.yearsworked = 3;
    taylor.salary = 50000;
    // Printing data
    printf("Employee's Name: %s\n",taylor.name);
    printf("Designation: %s\n",taylor.designation);
    printf("Address: %s\n",taylor.address);
    printf("Salary: %d\n",taylor.salary);
    printf("No. Of Years: %d\n",taylor.yearsworked);
    return 0;

}
int main(){
    printf("Assignment 17: Using Structures\n");
    runner();
    return 0;
}