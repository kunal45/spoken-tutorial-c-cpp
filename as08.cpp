#include <iostream>
// For Defining Precision point using InBuilt library iomanip
//#include <iomanip>
// For Defining Precision point using Inbuilt header cstdio
// #include <cstdio>
using namespace std;

int main(){
    int a, b, c, d;
    float sum;
    cout << "Assignment 8: Solving (a/b)+(c/d) using Typecasting";
    cout << "\nEnter the values for a, b, c, d: ";
    cin >> a >> b >> c >> d;
    // Performs Real Division & Explicit typecasts int to float
    sum = ((float)a/b + (float)c/d);
    cout<<"\nThe calculated value is: "<<sum;

    // Define Precision point if Required using iomanip
    // cout << "\n"<<std::fixed<<std::setprecision(3) << sum;
    // Define Precision point using cstdio
    // printf("The Calculated Value is %.3f",sum);
    return 0;
}