#include <iostream>
using namespace std;

// Function to do comparison
// C/C++ are case sensitive languages hence
// avoid using A with a & etc
int comp(int a, int b, int c){
    cout<<"\nNo. are: "<<a<<" "<<b<<" "<<c<<endl;
    /*
        Equal(==) cases are considered first than (>/<) cases to avoid
        cases where a & c are greater than b will result in output of
        c being Largest
    */
    if (a==b){
        if (a==c){
            cout<<"All No. Are Equals";
        }
        else if (a>c){
            cout<<"a & b are Equal and are Greatest";
        }
        else {
            cout<<"c is Greatest";
        } 
    }
    else if (b==c){
        if (a>b){
            cout<<"a is Greatest";
        }
        else {
            cout<<"b & c are Equal and are Greatest";
        }
    }
    else if (a==c){
        if (a>b){
            cout<<"a & c are Greatest";
        }
        else {
            cout<<"b is Greatest";
        }       
    }
    else if (a>b){
        if(a>c){
            cout<<"a is largest";
        }
        else{
            cout<<"c is largest";
    }
    }
    else if (b>a){
        if(b>c){
            cout<<"b is largest";
        }
        else{
            cout<<"c is largest";
        }
    }
    return 0;
}

int main(){
    int a,b,c;
    cout<<"Assignment 6: Q.2 Finding Greatest Number between 3 Numbers";
    cout<<"\nEnter the three numbers: ";
    cin>>a>>b>>c;
    comp(a,b,c);
    return 0;
}