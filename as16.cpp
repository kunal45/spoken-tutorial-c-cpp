#include <iostream>
#include <cstring>
//This header file defines several functions to manipulate C strings and arrays.
using namespace std;

int main(){
    char astring1[]="best \0", astring2[]="bus\0";
    cout<<"Assignment 16: Concatenate 2 Strings";
    cout<<"\nThe First String is: "<<astring1;
    cout<<"\nThe First String is: "<<astring2;
    // Using Strcat function from cstring. 
    // Permanently Concatenate astring1
    cout<<"\n\nUsing Strcat: "<<strcat(astring1,astring2);
    char astring3[]="best \0", astring4[]="bus\0";
    /*
        Implementing char array is faster than string.

        IF string operations are required to be done
        Instead of using char array and converting it to String
        Its better to use: string s1 = "best", s2="bus"; 
        and avoid conversion
        
        Here, I have used char to show how they can be converted 
        to string & also to maintain the C-isque part of
        the code as the problem was asked for C.


        One more Reason to avoid array when using string
        is to avoid array decay.
        Array Decay -  When array are passed into function by value
        or pointers, the first address of the array which is a pointer
        is sent. So in case sizeof is used, for an int array with 5 elements
        then its size would be 5*4=20. But due to Array Decay, the sizeof 
        one pointer will be printed. The size of one Pointer on 32-bit
        CPU is 32bits/8=4byte & on 64-bit CPU it is 64bit/8=8byte.
        Pointers takes so many byte to that it can store any address on
        the computer.

        TO avoid Array Decay, we might 
    */
    string s1 = astring3;
    string s2(astring4);
    /*
        Both string s1 = x & string s1(x) will convert the X char array
        into a string
    */
    cout<<"\nUsing + Operator: "<<s1+s1;
    // Permanently Concatenates S1
    // it is much faster than using +
    cout<<"\nUsing append: "<<(s1.append(s2));

    // Exploring Other Functions of string library
    // strncat
    char str1[20];
    char str2[20];
    strcpy (str1,"To be ");
    strcpy (str2,"or not to be");
    strncat (str1, str2, 6);
    cout<<"\nstrncat Usage:" <<str1;
    return 0;
}