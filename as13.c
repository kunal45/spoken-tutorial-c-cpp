#include <stdio.h>
// stdlib to get absolute difference between the number
#include <stdlib.h>

int main(){
    int arr[9],diff[4];
    // though the Question ask for difference b/w elements in array
    // 2 elements are used to have some meaningful result as
    // we are using large array
    printf("Assignment 13: Difference of 2 Elements Stored in Array");
    printf("\nEnter 10 numbers: ");
    for (int i = 0; i <10; i++){
        scanf("%d",&arr[i]);
    }
    for (int j = 0; j <10; j+=2){
        int k=0;
        // to get absolute difference between the numbers
        diff[k] = abs(arr[j+1]-arr[j]);
        printf("\nDifference between %d & %d consecutive number is: %d", j+1, j+2,diff[k]);
        k++;
    }
    return 0;
}