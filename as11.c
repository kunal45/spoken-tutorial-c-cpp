#include <stdio.h>

int main(){
    int a, b;
    printf("Assignment 11: Comparing 2 Numbers Using Not(!) Operator");
    printf("\nEnter the 2 numbers: ");
    scanf("%d %d", &a,&b);
    // != Checks for Being not equal
    if(a!=b){
        printf("\na is not equal to b");
    }else{
        printf("a is equal to b");
    }
    return 0;
}