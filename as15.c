#include <stdio.h>
#include <string.h>

int main(){
    /*
        \0 is used to declare End of the string
        In case \0 is not to be used, we can use str1[n]
        where n >= 7 
    */
    printf("Assignment 15: Printing a String\n");
    char str1[]={'S','t','r','i','n','g','\0'};
    printf("%s",str1);
    return 0;
}