#include <iostream>
#include <cmath>
using namespace std;

int main(){
    float num, sqr, sqrinb;
    cout<<"Assignment 4: Calculating Square of A Number";
    cout<<"\nEnter the Number to be Squared: ";
    cin>>num;
    // pow function computes power of x. 
    // pow(x,y) == x^y (i.e y raised to x, x to the power y)
    sqrinb = pow(num, 2);
    sqr = num*num; 
    cout << "\nSquare of the Number using Inbuilt Library CMath: "<<sqrinb;
    cout << "\nSquare of the Number using Formula: "<<sqr;
    return 0;
}