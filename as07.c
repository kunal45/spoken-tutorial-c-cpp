#include <stdio.h>
#include <math.h>

int caseswitch(float age){
    float exp=age/10;    
    int num;
    if (exp>6){
        // float is getting typecasted to int - Implicit Typecasting
        num = ceil(exp);
        // ceil will round to nearest int which is greater than or equal to value passed
        // this is used to avoid cases for age 61-64 where int exp would result in 6
    }
    else{
        num = floor(exp);
        // floor will round to nearest int which less than or equal to value passed
    }
    printf("\nUsing Switch: ");
    switch (num){
    case 2:
        printf("\nAge is b/w 20 to 60");
        break;
    case 3:
        printf("\nAge is b/w 20 to 60");
        break;
    case 4:
        printf("\nAge is b/w 20 to 60");
        break;
    case 5:
        printf("\nAge is b/w 20 to 60");
        break;
    case 6:
        printf("\nAge is b/w 20 to 60");
        break;    
    default:
        printf("\nAge is not in Range. You are fired!");
        break;
    }
    return 0;
}
int caseif(float age){
    printf("\nUsing If:");
    if((age>=20)&&(age<=60)){
        printf("\nAge is b/w 20 to 60");
    }
    else{
        printf("\nAge is not in Range. You are fired!");
    }
    return 0;
}

int main(){    
    float age;
    printf("Assignment 7: Age Verification for Employee");
    printf("\nEnter The Age: ");
    scanf("%fd",&age);
    caseswitch(age);
    caseif(age);
    return 0;
}