#include <iostream>
using namespace std;

int main(){
    cout<<"Assignment 12: Printing 0-9 using For loop"<<endl;
    // Simple for loop Eg.
    for (int i = 0; i < 10; i++){
        cout<<i<<" ";
    }
    return 0;
}