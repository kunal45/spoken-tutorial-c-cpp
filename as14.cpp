#include <iostream>
using namespace std;

int main(){
    int arr1[4][4], arr2[4][4], i,j;
    cout<<"Assignment 14: Subtracting 2 2-D Arrays";
    cout<<"\nEnter The Value for 1st 4x4 Array: ";
    for (i = 0; i < 4; i++){
        for ( j = 0; j < 4; j++){
            cin>>arr1[i][j];
        }
        
    }
    cout<<"\nEnter The Value for 2nd 4x4 Array: ";
    for (i = 0; i < 4; i++){
        for (j = 0; j < 4; j++){
            cin>>arr2[i][j];
        }
        
    }
    cout<<"\nThe Subracted Array is: \n";
    for (i = 0; i < 4; i++){
        for (j = 0; j < 4; j++){
            cout<<(arr1[i][j]-arr2[i][j])<<"\t";
        }
        cout<<"\n";
    }
    return 0;
}