#include <iostream>
using namespace std;

int main(){
    cout<<"Assignment 18: Printing variable Address using Pointers";
    int *ptr, var=9;
    ptr = &var;
    cout<<"\nUsing pointer to print address of variable: "<<ptr;
    cout<<"\nDirectly printing address of variable: "<<&var;

    // Dereferencing a pointer. The value of Variable
    // Pointed is returned. This is done * operator.
    // It is dereference/indirection operator 
    cout<<"\nUsing pointer to print value of a variable: "<<*ptr;
    cout<<"\nPrinting value of a variable: "<<var;
    return 0;
}