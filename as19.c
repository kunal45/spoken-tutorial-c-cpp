#include <stdio.h>
#include <math.h>

// the value of a is copied to x
// this is an eg. of pass by value
// Value of a doesn't change
int cubeit(int x){
    int cube1,cube2;
    cube1 = x*x*x;
    printf("\nThe Cube value of %d is: %d", x, cube1);
    // pow function computes power of x. 
    // pow(x,y) == x^y (i.e y raised to x, x to the power y)
    cube2 = pow(x,3);
    printf("\nThe Cube value of %d using pow is: %d", x, cube2);
    return 0;
}

int main(){
    int a;
    printf("Assignment 19: Cubing values using pass by value");
    printf("\nPlease Enter a integer number to be cubed: ");
    scanf("%d",&a);
    // Passing the value of a to cubeit function
    cubeit(a);
    return 0;
}