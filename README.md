# Spoken Tutorial - C & C++ - Assignment Answers

 
This Repo contains all the 20 Assignment answers for the [C & C++  course](https://spoken-tutorial.org/tutorial-search/?search_foss=C%20and%20Cpp&search_language=English).

The codes have been tested and compiled using gcc v8.1.0 & g++ v8.1.0. 

For the Assignments, instead of doing all the assignments in both languages, I have alternated between them for each assignment as there was only common changes between each of them and thus wasn't required

### Therefore, all the:

 - Odd no. assignments are written in C (1,3,5..)
 - Even no. assignments are written in C++. (2,4,6..)

### The Following naming scheme is used for the code:

- as01.c - Here, 'as' stands for assignment, and '01' denotes the lecture name.  
- as06-1.cpp - Here the -1 denotes Q.1 for the particular assignment.

Note: Please use the code under this repository for refernce only
      It is highly advisable to try each Assignment before checking the solutions here.
