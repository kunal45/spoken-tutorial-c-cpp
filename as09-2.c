#include <stdio.h>

int main(){
    int a, b, c;
    printf("Assignment 9: Q.2 Printing Remainder using Modulus");
    printf("\nEnter the Dividend(a) & Divisor(b): ");
    scanf("%d %d",&a,&b);
    // modulus Operator - %
    // Used to get remainder
    c = a%b;
    printf("\nRemainder of a & b is: %d",c);
    return 0;
}