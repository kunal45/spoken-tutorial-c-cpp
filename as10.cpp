#include <iostream>
using namespace std;

// The below function is being reused with minor changes from as6-2
int studcomp(int a, int b, int c){
    cout<<endl;
    if (a==b){
        if (a==c){
            cout<<"All Students Scored Equal Marks";
        }
        else if (a>c){
            cout<<"Student 1 & Student 2 are Equal and have highest harks";
        }
        else {
            cout<<"Student 3 has highest marks";
        } 
    }
    else if (b==c){
        if (a>b){
            cout<<"Student 1 has highest marks";
        }
        else {
            cout<<"Student 2 & Student 3 are Equal and have highest marks";
        }
    }
    else if (a==c){
        if (a>b){
            cout<<"Student 1 & Student 3 have highest marks";
        }
        else {
            cout<<"Student 2 has highest marks";
        }       
    }
    else if (a>b){
        if(a>c){
            cout<<"Student 1 has highest marks";
        }
        else{
            cout<<"Student 3 has highest marks";
    }
    }
    else if (b>a){
        if(b>c){
            cout<<"Student 2 has highest marks";
        }
        else{
            cout<<"Student 3 has highest marks";
        }
    }
    return 0;
}


int main(){
    int stud1, stud2, stud3;
    cout<<"Assignment 10: Comparing Marks of 3 Students";
    cout<<"\nEnter The Marks for First Student: ";
    cin>>stud1;
    cout<<"\nEnter The Marks for Second Student: ";
    cin>>stud2;
    cout<<"\nEnter The Marks for Third Student: ";
    cin>>stud3;
    studcomp(stud1,stud2,stud3);
    return 0;
}