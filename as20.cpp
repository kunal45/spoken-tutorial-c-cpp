#include <iostream>
#include <fstream>
// Cpp Header for file handling. Using the following 2:
// ofstream - Write on files
// ifstream - Read from files
using namespace std;

int main(){
    cout<<"Assignment 20: Creating, Writing & Reading from a file";
    // Declaring it as it will be used for Printing Purpose
    string printline;
    // Create object with name filenew of ofstream class
    ofstream filenew;
    // By default out mode is used.
    // In out mode, the content of the files are deleted
    // an then written. To write at EOF use apt or app
    // 2+ modes can be used at same time using OR - | 
    // Opens the file/Create if not Existing
    filenew.open("TEST.txt", ios::out);
    cout<<"\nWriting to file\n";
    // Content Written to the file
    filenew<<"My Name is:  \nMy Address is: ";
    // File Closed
    filenew.close();    
    

    // Reading file. Creates an object ifilenew of ifstream class
    ifstream ifilenew("TEST.txt");

    cout<<"\nReading from the file\n";

    // Using a while loop together with the getline() function 
    // to read the file - line by line until EOF(End of FIle)
    while (getline (ifilenew, printline)) {
    // Prints the line in console
        cout << printline<<endl;
    }
    // Closing file
    ifilenew.close();
    
    return 0;
}